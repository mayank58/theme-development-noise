import React ,{Component} from "react"
/* Plugins */
import { Switch, Route } from 'react-router-dom';
// component
import HomePage from "./pages/home-page/HomePage";

class App extends Component{
    render(){
        return(
            <>
            <div className="main-content">
                <Switch>
                <Route exact component={HomePage} path='/' />
                </Switch>

            </div>
            </>
        )
    }
}

export default App;