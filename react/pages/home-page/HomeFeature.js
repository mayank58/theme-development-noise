import React, { Component } from 'react';
import UrlSet from "../../component/UrlSet";
import SrcSet from "../../component/SrcSet";
import HtmlParser from 'react-html-parser'
class HomeFeature extends Component {
    render() {
        const {section}=this.props
        const block=section.block
        return (<>
        <div className="home-feature-colum" id={`section_${section.sectionId}`}>
            <div className={section.settings.home_fearure_full_width?"full-width":"container"}>
            <div className="flex-view-xs">
                {
                    block.length >0?
                    block.map((item,index)=>{
                        return(
                            <div className="col-sm-6 col-xs-12" key={index}>
                            <div className="feature-wrapper">
                                <div className="title-wraper">
                                    <h2>{item.home_feature_title}</h2>
                                    </div>
                                    <div className="feature-dec">
                                        <h5>{HtmlParser(item.home_fearure_richtext)}</h5>
                                        </div>
                                        {
                                            item.home_fearure_btn_text?
                                            <div className="feature-btn-wrapper">
                                            <UrlSet href={item.home_fearure_btn_url} className="text-link">
                                                {item.home_fearure_btn_text}
                                                <img className="icon-img" src={pwa.icons.peachRightArrow } />
                                                </UrlSet>
                                               
                                            </div>:""
                                        }
                                        <div className="feature-image">
                                        <UrlSet href={item.home_fearure_btn_url}>
                                            <SrcSet src={item.home_fearure_image} alt={'image-alt'}/>
                                            </UrlSet>

                                            </div>
                                </div>

                        </div>
                        )
                    })
                    :null
                }
            </div>
            </div>
            </div>
            </>
        );
    }
}

export default HomeFeature;