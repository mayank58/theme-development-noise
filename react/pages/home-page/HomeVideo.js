import React, { Component } from 'react';
import MediaQuery from "../../component/MediaQuery";
import Modal from 'react-modal';
class HomeVideo extends Component {
    constructor(props){
        super(props);
        this.state={
            modalIsOpen:false
        }
        this.customeStyle={
            content: {
                top: "50%",
                left: "50%",
                right: "auto",
                bottom: "auto",
                marginRight: "-50%",
                transform: "translate(-50%, -50%)",
                position:"absolute",
                backgroundColor: "#000",
                maxWidth:'600px',
                padding: '0px',
                width: "calc(100% - 20px)"
              }
        }
    }
    videoModal=(value)=>{
        this.setState({modalIsOpen:value})
    }
    render() {
        const { section } = this.props;
        const sectionId = section.sectionID;
        const {home_video_url,home_video_mobile_url,home_image,home_image_mobile,home_video_controls,home_video_autoplay,home_video_loop,
            home_video_mute,home_video_heading,home_video_subheading,home_video_btn_txt,home_video_btn_url}=section.settings
        console.log(section.settings,"section.settings");
        return (<>
            <div className="home-video" id={`section_${sectionId}`}>
                <div className="home-video-wrapper">
                    <div className="home-video-text">
                        <div className="video-title">
                            <h2>{home_video_heading}</h2>
                        </div>
                        <div className="video-subheading">
                            <h5>{home_video_subheading}</h5>
                        </div>
                        <div className="video-button-wrapper">
                            <button className="btn video-button" onClick={()=>this.videoModal(true)}>{home_video_btn_txt}</button>
                        </div>
                    </div>
                    <div className="video-wrapper">
                        <MediaQuery query="phone">
                        <video 
                        playsInline 
                        controls={home_video_controls} 
                        muted={home_video_mute}
                        autoPlay={home_video_autoplay} 
                        loop={home_video_loop} 
                        width="100%" 
                        height="100%">
                            <source src={home_video_mobile_url}></source>
                        </video>
                        </MediaQuery>
                        <MediaQuery query="tablet-and-up">
                        <video
                           playsInline 
                           controls={home_video_controls} 
                           muted={home_video_mute}
                           autoPlay={home_video_autoplay} 
                           loop={home_video_loop} 
                           width="100%" 
                           height="100%">
                            <source src={home_video_url}></source>
                        </video>
                        </MediaQuery>

                    </div>
                </div>
            </div>
            {
                home_video_btn_url &&  <Modal
                isOpen={this.state.modalIsOpen}
                style={this.customeStyle.content}
              
    
                >
           <div className="video-modal-wrapper">
              <iframe
                playsInline
                width="100%"
                height="100%"
                src={home_video_btn_url}
              >
              </iframe></div>
                </Modal>
            }
           
            </>
        );
    }
}

export default HomeVideo;