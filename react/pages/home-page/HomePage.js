import React ,{ Component } from "react"
import HomeSlider from "./HomeSlider";
import HomeFeature from "./HomeFeature";
import HomeBanner from "./HomeBanner";
import HomeInPress from "./HomeInPress";
import HomeVideo from "./HomeVideo";
class HomePage extends Component{
    render(){
        const homePageHtlm=document.getElementsByClassName("section-data");
        const dataJson=[]
        Array.prototype.forEach.call(homePageHtlm,(el)=>{
            let obj={};
            obj.type=el.getAttribute('data-type');
            obj.data=JSON.parse(el.innerHTML);
            dataJson.push(obj)
        })
        console.log(dataJson,'dataJson');
        return(
            <div className="home-page">
           {
               dataJson.length > 0 ?
               dataJson.map((item,index)=>{
                   switch (item.type) {
                       case "home-slider":
                       return <HomeSlider key={item.data.sectionId} section={item.data}/>;
                       case "home-feature-colum":
                           return <HomeFeature key={item.data.sectionId} section={item.data}/>;
                        case "home-idc-banner":
                            return <HomeBanner key={item.data.sectionId} section={item.data}/>;
                        case "home-in-press":
                            return <HomeInPress key={item.data.sectionId} section={item.data}/>;
                        case "home-video":
                                return <HomeVideo key={item.data.sectionId} section={item.data}/>;
                       default:
                        return "";
                   }
               })
               :null 
           }
            </div>
        )
    }
}

export default HomePage;