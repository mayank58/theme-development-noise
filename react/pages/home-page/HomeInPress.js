import React, { Component } from 'react';
import Srcset from "../../component/SrcSet";
class HomeBanner extends Component {
    render() {
        const { section } = this.props;
        const sectionId = section.sectionID;
        const block = section.block;
        const { press_title, press_text } = section.settings
        console.log("object");
        return (
            <div className="home-in-press" id={`section_${sectionId}`}>
                <div className="container">
                    <div className="press-title-wrapper">
                        <div className="press-title">
                            <h2>{press_title}</h2>
                        </div>
                        <div className="press-text">
                            <p>{press_text}</p>
                        </div>
                    </div>
                    <div className="flex-view-xs">
                        {
                            block.map((item, index) => {
                                return <div className="col-sm-4 col-xs-6">
                                    <div className="bloack-content">
                                        <div className="press-image">
                                            <Srcset src={item.press_image_desktop} />
                                        </div>
                                    </div>
                                </div>
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default HomeBanner;