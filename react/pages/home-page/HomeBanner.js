import React, { Component } from 'react';
import Srcset from "../../component/SrcSet";
import HtmlParser from 'react-html-parser'
class HomeBanner extends Component {
    render() {
        const { section } = this.props;
        const sectionId = section.sectionID
        const { idc_banner_image, idc_banner_text, idc_banner_title, idc_image_url } = section.settings
        return (
            <div className="home-idc-banner" id={`section_${sectionId}`}>
                <div className="container">
                    <div className="home-banner-wrapper">
                        <div className="home-banner-left">
                          <div className="banner-text">
                            <div className="title">
                                <h5>{idc_banner_title}</h5>
                            </div>
                            <div className="discription">
                                {HtmlParser(idc_banner_text)}
                            </div>
                          </div>
                          <div className="banner-image">
                              <Srcset src={idc_image_url} />
                          </div>
                        </div>
                        <div className="home-banner-right">
                        <Srcset src={idc_banner_image} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default HomeBanner;