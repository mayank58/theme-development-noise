import React, { Component } from "react"
import Slider from "react-slick";
import MediaQuery from "../../component/MediaQuery";
import UrlSet from "../../component/UrlSet";
import Srcset from "../../component/SrcSet";
import LazyLoad from 'react-lazyload';
class HomeSlider extends Component {
    render() {
        const { section } = this.props;
        const blocksId = section.blocksId;
        let settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000
        }
        return (
            <div className="slideshow-wrapper" id={`section_${section.sectionId}`}>
                {/* <iframe width="100%" height="600" src="http://www.youtube.com/embed/mbLgK_MnuHA?autoplay=1" frameborder="0" allowfullscreen></iframe> */}
                <div className={section.settings.full_width?"full-width":"container"}>
                    <div className="slickSlider">
                        <Slider {...settings} >
                            {section.blocks.map((item, index) => {
                                return (
                                    <div className={`single-item block-${blocksId[index]}-${index + 1}`} key={blocksId[index]}>
                                        <div className="image-wrapper">
                                            <UrlSet href={item.slider_link} className={'img-link'} >
                                                <MediaQuery query="lap-and-up">
                                                    {item.slider_image &&
                                                            <Srcset alt="img" src={item.slider_image} />
                                                    }
                                                </MediaQuery>
                                                <MediaQuery query="phone-and-tablet">
                                                    {item.slider_image &&
                                                            <Srcset alt="img" src={item.slider_image_mobile} />
                                                    }
                                                </MediaQuery>
                                            </UrlSet>
                                        </div>
                                    </div>
                                )

                            })
                            }

                        </Slider>
                    </div>
</div>
                </div>
        )
    }
}
export default HomeSlider;